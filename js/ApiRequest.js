var ApiRequest=function (method) {
    this.method=method;
    //this.url=url
};

ApiRequest.prototype.createObject=function () {
    var request;
    try {
    // Mozilla,Safari,IE7
    request = new XMLHttpRequest();
        //console.log("created XMLHttpRequest")
    } catch (e) {
    // IE
    var request_ids = [
        'MSXML2.XMLHTTP.5.0',
        'MSXML2.XMLHTTP.4.0',
        'MSXML2.XMLHTTP.3.0',
        'MSXML2.XMLHTTP',
        'Microsoft.XMLHTTP'
    ];
         var success = false;
         for (var i=0;i < request.ids.length && !success; i++) {
            try {
            request = new ActiveXObject(request.ids[i]);
            success = true;
            } catch (e) {}
         }
            if (!success) {
            throw new Error('Unable to create XMLHttpRequest.');
            }
    }
    return request;
};

ApiRequest.prototype.makeRequest=function (url,payload,headers) {
    var request = this.createObject();
if(this.method==="GET") {
headers=payload;
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            var loaded=clietnsData;
            loaded(request.responseText);
            console.log("done" /*+request.responseText*/);
        }
    };
    request.open(this.method, url, true);
    for (var key in headers) {
        request.setRequestHeader(key, headers[key]);
    }
    request.send();

}else if(this.method==="POST"){
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            console.log("done" /*+request.responseText*/);
        };
    };
    request.open(this.method, url, true);
    for (var keys in headers) {
        request.setRequestHeader(keys, headers[keys]);
    }
    request.send(payload);
}
};

get=new ApiRequest("GET");
get.makeRequest(Config.clients_url,{"Accept-Language":"en-US"});

post=new ApiRequest("POST");
post.makeRequest(Config.clients_url,'user=person&pwd=password',{"Accept-Language":"en-US"});
